var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
const time = 10000
// readBooks(books.timeSpent, books.name, sisaWaktu)
readBooks(time, books[0], (timeLeft0) => {
    console.log('Waktu yang tersisa adalah ' + timeLeft0)

    readBooks(timeLeft0, books[1], (timeLeft1) => {
        console.log('Waktu yang tersisa adalah ' + timeLeft1)
   
        readBooks(timeLeft1, books[2], (timeLeft2) => {
            console.log('Waktu yang tersisa adalah ' + timeLeft2)
       
            readBooks(timeLeft2, books[3], (timeLeft3) => {
                console.log('Waktu yang tersisa adalah ' + timeLeft3)
            })
        })
    })
})
