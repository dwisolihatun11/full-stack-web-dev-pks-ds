// Soal 1
/*
    buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini 
*/

// const luas = (sisi) => {
//     console.log("Luas Persegi dengan sisi " + sisi + " adalah " + sisi * sisi)
// }

// const keliling = (sisi) => {
//     console.log("Keliling Persegi dengan sisi " + sisi + " adalah " + sisi * 4)
// }

// luas(10)
// keliling(10)

const luas = (panjang, lebar) =>{
    console.log(panjang * lebar)
}

const keliling = (panjang, lebar) => {
    console.log(2 * (panjang + lebar))
}

luas(10, 7)
keliling(10, 7)


// Soal 2
/*
    Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana

    const newFunction = function literal(firstName, lastName){
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function(){
        console.log(firstName + " " + lastName)
        }
    }
    }
    
    //Driver Code 
    newFunction("William", "Imoh").fullName() 
*/

const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName : () => {
            console.log(firstName + " " + lastName)
        }
    }
}
    
    //Driver Code 
    newFunction("William", "Imoh").fullName()
    
   


// Soal 3

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football"
  }

// Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)
const {firstName, lastName, address, hobby} = newObject

// Driver code
console.log(firstName, lastName, address, hobby)

// Soal 4 
// Kombinasikan dua array berikut menggunakan array spreading ES6
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
const combined = [...west, ...east]
//Driver Code
console.log(combined)

// Soal 5
/* sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:
    const planet = "earth" 
    const view = "glass" 
    var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
*/

const planet = "earth" 
const view = "glass"

const planetView = {planet, view}
console.log(planetView)

