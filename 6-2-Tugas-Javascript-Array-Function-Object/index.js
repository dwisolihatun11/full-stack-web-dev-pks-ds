// Soal 1
/*
buatlah variabel seperti di bawah ini

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):

1. Tokek
2. Komodo
3. Cicak
4. Ular
5. Buaya
*/

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
hewan = daftarHewan.sort()
hewan.forEach(function(hewan){
    console.log(hewan)
})


// Soal 2

/*
Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 
*/

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
function introduce(biodata)
{
    return "Nama saya " + biodata.name + ", umur saya " + biodata.age + " tahun, alamat saya di " + biodata.address + ", dan saya punya hobby yaitu " + biodata.hobby
}

var perkenalan = introduce(data)
console.log(perkenalan)


// Soal 3

/*
Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2
*/

function hitung_huruf_vokal(kata)
{
    // var vokal = ["a", "i", "u", "e", "o"]
    var vokal = "aiueoAIUEO"
    var hitung = 0

    for (var i = 0; i < kata.length; i++) {
        if(vokal.indexOf(kata[i]) != -1)
        {
            hitung++
        }       
    }

    return hitung
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

// Soal 4

/*
    Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.

    console.log( hitung(0) ) // -2
    console.log( hitung(1) ) // 0
    console.log( hitung(2) ) // 2
    console.log( hitung(3) ) // 4
    console.log( hitung(5) ) // 8
*/

function hitung(index)
{
    return index * 2 - 2
}

console.log( hitung(0) ) 
console.log( hitung(1) )
console.log( hitung(2) ) 
console.log( hitung(3) )
console.log( hitung(5) )