<?php

/**
 * Memiliki Property nama, darah dengan nilai default 50, jumlahKaki, dan keahlian.
 */
trait Hewan
{
    public $name;
    public $darah = 50,
        $jumlahKaki,
        $keahlian;


    // di dalam method ini akan menampilkan string nama dan keahlian
    public function atraksi($name, $keahlian)
    {
        echo $name . " sedang " . $keahlian;
    }
}

/**
 * Memiliki Property attackPower, dan defencePower
 */
trait Fight
{
    use Hewan;
    public $attackPower, // Serangan
        $defencePower;  // Mempertahankan 

    // di dalam method ini akan menampilkan string sebagai contoh berikut. Contoh : "harimau_1 sedang menyerang elang_3" atau "elang_3 sedang menyerang harimau_2".
    public function serang($name1, $name2, $attackPower, $defencePower)
    {
        $this->name = [$name1, $name2];
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
        echo $this->name[0] . " sedang menyerang " . $this->name[1] . "<br>";
        $this->diserang($this->name[1], $this->attackPower, $this->defencePower);
    }

    public function diserang($name, $attackPower, $defencePower)
    {
        $this->name = $name;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
        $this->darah = $this->darah - $attackPower / $defencePower;
        echo $this->name . " sedang diserang ";
    }
}

class Elang
{
    use Fight;

    public function getInfoHewan($keahlian, $jumlahKaki, $attackPower, $defencePower)
    {
        $this->keahlian = $keahlian;
        $this->name = ['Elang', 'Harimau'];
        $this->jumlahKaki = $jumlahKaki;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
        echo  $this->atraksi($this->name[0], $this->keahlian) . " dengan jumlah kaki : " . $this->jumlahKaki . "<br>";
        echo $this->serang($this->name[1], $this->name[0], $this->attackPower, $this->defencePower) . "<br>Jumlah darah " . $this->name . " saat ini tersisa : " . $this->darah;
    }
    //didalam method ini semua property yang ada di dalam kelas Hewan dan Fight ditampilkan , dan jenis hewan (Elang atau Harimau)
}

class Harimau
{
    use Fight;

    //didalam method ini semua property yang ada di dalam kelas Hewan dan Fight ditampilkan , dan jenis hewan (Elang atau Harimau)
    public function getInfoHewan($keahlian, $jumlahKaki, $attackPower, $defencePower)
    {
        $this->keahlian = $keahlian;
        $this->name = ['Harimau', 'Elang'];
        $this->jumlahKaki = $jumlahKaki;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
        echo  $this->atraksi($this->name[0], $this->keahlian) . " dengan jumlah kaki : " . $this->jumlahKaki . "<br>";
        echo $this->serang($this->name[1], $this->name[0], $this->attackPower, $this->defencePower) . "<br>Jumlah darah " . $this->name . " saat ini tersisa : " . $this->darah;
    }
}

$elang = new Elang();
$elang->getInfoHewan('Terbang Tinggi', 2, 10, 5);
echo "<br>";
echo "<br>";
$harimau = new Harimau();
$harimau->getInfoHewan('Lari Cepat', 4, 7, 8);
