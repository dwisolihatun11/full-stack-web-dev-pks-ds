<?php

namespace App;

use Illuminate\support\Str;
use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable = ['content', 'post_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName('id')})) {
                $model->{$model->getKeyName('id')} = Str::uuid();
            }

            $model->users_id = auth()->user()->id;
        });
    }

    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    public function users()
    {
        return $this->belongsTo('App\Users');
    }
}
