<?php

namespace App\Mail;

use App\Users;
use App\OtpCode;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserMail extends Mailable
{
    use Queueable, SerializesModels;


    public $user;
    // public $otp_code;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Users $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $id = $this->user->id;
        $otp_code = OtpCode::where('users_id', $id)->first();
        return $this->view('mails.user.register', compact('otp_code'));
    }
}
