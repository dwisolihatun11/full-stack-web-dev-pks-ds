<?php

namespace App;

use Illuminate\support\Str;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['judul', 'deskripsi', 'users_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName('id')})) {
                $model->{$model->getKeyName('id')} = Str::uuid();
            }

            $model->users_id = auth()->user()->id;
        });
    }

    public function comments()
    {
        return $this->hasMany('App\Comments');
    }

    public function users()
    {
        return $this->belongsTo('App\Users');
    }
}
