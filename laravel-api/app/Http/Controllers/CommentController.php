<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
use App\Mail\CommentAuthorMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only([
            'store', 'update', 'delete'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comment = Comments::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'List Data Comment',
            'data' => $comment
        ], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required',
            'post_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comments::create([
            'content' => $request->content,
            'post_id' => $request->post_id
        ]);

        // Dikirim kepada yang memiliki Postingan
        Mail::to($comment->post->users->email)->send(new PostAuthorMail($comment));
        
        // Dikirim kepada yang membuat Comment
        Mail::to($comment->users->email)->send(new CommentAuthorMail($comment));

        if ($comment) {
            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data' => $comment
            ], 201);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment Failed Save to Database'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comments::findOrfail($id);
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Comment',
            'data' => $comment
        ], 200);
    }

    public function update(Request $request, Comments $comment)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comments::findOrfail($comment->id);

        if ($comment) {
            $user = auth()->user();

            if ($comment->users_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Comment bukan milik anda!'
                ], 403);
            }

            $comment->update([
                'content' => $request->content,
                'request' => $request->post_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data' => $comment
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comments::findOrfail($id);
        if ($comment) {

            $user = auth()->user();

            if ($comment->users_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Comment bukan milik anda!'
                ], 403);
            }

            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted'
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found'
        ], 404);
    }
}
