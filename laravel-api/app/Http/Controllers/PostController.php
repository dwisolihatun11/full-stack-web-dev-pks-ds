<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth:api')->only([
            'store', 'update', 'delete'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get data form table posts
        $post = Post::latest()->get(); // ngambil dari urutan terakhir

        // make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Post',
            'data' => $post
        ], 200);
    }

    public function store(Request $request)
    {
        // set validation
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'deskripsi' => 'required'
        ]);

        //  response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        // save to database
        $post = Post::create([
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi
        ]);

        // success save to database
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post Created',
                'data' => $post
            ], 201);
        }

        // failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Post Failed to Save'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // find post by id
        $post = Post::findOrfail($id);

        //  make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data' => $post
        ], 200);
    }

    public function update(Request $request, Post $post)
    {
        // set validation
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'deskripsi' => 'required'
        ]);

        // response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        // find post by id
        $post = Post::findOrfail($post->id);

        
        if ($post) {
            // update post
            $user = auth()->user();

            if ($post->users_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Post bukan milik anda!'
                ], 403);
            }
            $post->update([
                'judul' => $request->judul,
                'deskripsi' => $request->deskripsi
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post Updated',
                'data' => $post
            ], 200);
        }

        // data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        // find post by id
        $post = Post::findOrfail($id);

        if ($post) {
            // delete post

            $user = auth()->user();

            if ($post->users_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Post bukan milik anda!'
                ], 403);
            }

            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Deleted'
            ], 200);
        }

        // data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found'
        ], 404);
    }
}
