<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\OtpCode;
use Carbon\Carbon;
// use App\Mail\UserMail;
use App\Events\UserStored;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd('Selamat Datang');
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
            'usernmae' => 'requiered|unique:users,username'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = Users::create($allRequest);

        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);

        $now = Carbon::now();
        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'users_id' => $user->id
        ]);


        // Memanggil event Register
        event(new UserStored($user));

        return response()->json([
            'success' => true,
            'message' => "Data User Berhasil Dibuat",
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ]);
    }
}
