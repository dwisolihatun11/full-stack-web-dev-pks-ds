<?php

namespace App\Http\Controllers;

use App\Roles;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Roles::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'List Data Roles',
            'data' => $roles
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $roles = Roles::create([
            'name' => $request->name
        ]);

        if ($roles) {
            return response()->json([
                'success' => true,
                'message' => 'Roles Created',
                'data' => $roles
            ], 201);
        }

        return response()->json([
            'success' => false,
            'message' => 'Roles Failed Save to Database'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roles = Roles::findOrfail($id);
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Roles',
            'data' => $roles
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Roles $roles)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $roles = Roles::findOrfail($roles->id);

        if ($roles) {
            $roles->update([
                'name' => $request->name
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Roles Updated',
                'data' => $roles
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Roles Not Found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $roles = Roles::findOrfail($id);
        if ($roles) {
            $roles->delete();

            return response()->json([
                'success' => true,
                'message' => 'Roles Deleted'
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Roles Not Found'
        ], 404);
    }
}
