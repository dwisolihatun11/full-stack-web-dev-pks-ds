<?php

namespace App\Listeners;

use App\Events\UserStored;
use App\Mail\RegenerateOtpMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegenerateOtpNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserStored  $event
     * @return void
     */
    public function handle(UserStored $event)
    {
        //  Kirim Email
        Mail::to($event->user->email)->send(new RegenerateOtpMail($event->user));
    }
}
