<?php

namespace App;

use Illuminate\support\Str;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $fillable = ['name'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName('id')})) {
                $model->{$model->getKeyName('id')} = Str::uuid();
            }
        });
    }

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
